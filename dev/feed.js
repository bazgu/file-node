var crypto = require('crypto'),
    http = require('http')

var ReadText = require('../lib/ReadText.js')

var config = require('../config.js')

var req = http.request({
    host: config.host,
    port: config.port,
    path: '/frontNode/feed?token=0_0123456789012345678901234567890123456789',
    method: 'post',
}, res => {
    ReadText(res, text => {
        console.log(text)
    })
})

;(() => {

    function next () {
        req.write(crypto.randomBytes(1024 * 2))
        setTimeout(next, 200)
    }

    next()

})()
