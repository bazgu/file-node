var http = require('http')

var ReadText = require('../lib/ReadText.js')

var config = require('../config.js')

var token = '0_0_0123456789012345678901234567890123456789'

var received = 0
var req = http.get({
    host: config.host,
    port: config.port,
    path: '/frontNode/receive?token=' + token,
}, res => {
    console.log('receiving')
    res.on('data', chunk => {
        received += chunk.length
        console.log('received', received)
    })
    res.on('end', () => {
        console.log('end')
    })
})
