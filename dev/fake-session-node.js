var http = require('http')

var Log = require('../lib/Log.js')

http.createServer((req, res) => {
    Log.http(req.url)
    res.setHeader('Content-Type', 'application/json')
    res.end(JSON.stringify({
        sendToken: '0_0123456789012345678901234567890123456789',
        name: 'sample.bin',
        size: 1024,
    }))
}).listen(7400, '127.0.0.1')
