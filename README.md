Bazgu File Node
===============

This program **file-node** is one of the nodes in a Bazgu infrastructure.
A file-node serves for transferring files from one user to another.

Workflow
--------

A user starts receiving a file from a file-node. The file-node fetches
the file details from the sender session-node and ask the sender
to feed the file. The sender then sends the file content chunk-by-chunk.
Each chunk is responded to the receiver.

Scripts
-------

* `./restart.sh` - start/restart the server.
* `./stop.sh` - stop the server.
* `./clean.sh` - clean the server after an unexpected shutdown.
* `./rotate.sh` - clean old logs.

Configuration
-------------

`config.js` contains the configuration.
