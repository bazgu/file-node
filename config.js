module.exports = {

    debug_mode: true,
    listen: {
        port: 7500,
        host: '127.0.0.1',
    },

    configNode: {
        host: '127.0.0.1',
        port: 7600,
    },

}
