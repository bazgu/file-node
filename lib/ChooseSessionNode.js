var FetchJson = require('./FetchJson.js')

module.exports = loadedConfig => {

    var sessionNodes = loadedConfig.sessionNodes.map(sessionNode => {
        return FetchJson(sessionNode.host, sessionNode.port)
    })

    return token => {
        var index = parseInt(token.split('_', 2)[1], 10)
        if (!isFinite(index) || index < 0 || index > sessionNodes.length - 1) return
        return sessionNodes[index]
    }

}
