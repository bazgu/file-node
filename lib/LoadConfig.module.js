const http = require('http')

module.exports = app => callback => {

    function errorListener (err) {
        app.CliError(logPrefix + err.code)
    }

    const configNode = app.config.configNode

    const host = configNode.host,
        port = configNode.port

    const logPrefix = 'config-node: ' + host + ':' + port + ': get: '

    const proxyReq = http.get({
        host, port,
        path: '/get',
    }, proxyRes => {

        proxyReq.removeListener('error', errorListener)

        const statusCode = proxyRes.statusCode
        if (statusCode !== 200) {
            app.CliError(logPrefix + 'HTTP status code ' + statusCode)
        }

        app.ReadText(proxyRes, responseText => {

            const response = (() => {
                try {
                    return JSON.parse(responseText)
                } catch (e) {
                    app.CliError(logPrefix + 'Invalid JSON document: ' + JSON.stringify(responseText))
                }
            })()

            callback(response)

        })

    })
    proxyReq.on('error', errorListener)

}
