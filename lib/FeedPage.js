module.exports = transfers => request => {

    function done () {
        transfer.endFeed()
        request.respond(true)
    }

    const { req, res } = request

    const transfer = transfers[request.parsed_url.query.token]
    if (transfer === undefined) {
        request.respond('INVALID_TOKEN')
        return
    }

    if (transfer.isBusy()) {
        request.respond('TRANSFER_BUSY')
        return
    }

    transfer.beginFeed(() => {
        request.respond('INVALID_TOKEN')
    })

    res.writeContinue()

    let ended = false
    let paused = false
    req.on('data', chunk => {

        if (chunk.length > transfer.left()) {
            transfer.endFeed()
            request.respond('TOO_MUCH_DATA')
            return
        }

        paused = true
        req.pause()
        transfer.feed(chunk, () => {
            paused = false
            if (ended) done()
            else req.resume()
        })

    })
    req.on('end', () => {
        if (paused) ended = true
        else done()
    })

}
