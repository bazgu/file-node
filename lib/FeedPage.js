module.exports = transfers => {
    return (req, res, parsedUrl) => {

        function done () {
            transfer.endFeed()
            res.end('true')
        }

        res.setHeader('Content-Type', 'application/json')

        var transfer = transfers[parsedUrl.query.token]
        if (transfer === undefined) {
            res.end('"INVALID_TOKEN"')
            return
        }

        if (transfer.isBusy()) {
            res.end('"TRANSFER_BUSY"')
            return
        }

        transfer.beginFeed(() => {
            res.end('"INVALID_TOKEN"')
        })

        res.writeContinue()

        var ended = false
        var paused = false
        req.on('data', chunk => {

            if (chunk.length > transfer.left()) {
                transfer.endFeed()
                res.end('"TOO_MUCH_DATA"')
                return
            }

            paused = true
            req.pause()
            transfer.feed(chunk, () => {
                paused = false
                if (ended) done()
                else req.resume()
            })

        })
        req.on('end', () => {
            if (paused) ended = true
            else done()
        })

    }
}
