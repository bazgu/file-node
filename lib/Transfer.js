module.exports = (size, chunkListener, destroyListener) => {

    function startTimeout () {
        timeout = setTimeout(destroyListener, 1000 * 10)
    }

    function stopTimeout () {
        clearTimeout(timeout)
    }

    var left = size
    var timeout = 0
    var abortListener = null
    var aborted = false
    startTimeout()

    return {
        abort: () => {
            aborted = true
            if (abortListener === null) stopTimeout()
            else abortListener()
        },
        beginFeed: _abortListener => {
            abortListener = _abortListener
            stopTimeout()
        },
        endFeed: () => {
            abortListener = null
            if (left !== 0) startTimeout()
        },
        feed: (chunk, deliverCallback) => {
            chunkListener(chunk, () => {
                if (aborted) return
                deliverCallback()
                left -= chunk.length
                if (left === 0) destroyListener()
            })
        },
        isBusy: () => {
            return abortListener !== null
        },
        left: () => {
            return left
        },
    }

}
