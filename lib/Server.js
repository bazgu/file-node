var http = require('http')

var Log = require('./Log.js')

var config = require('../config.js')

module.exports = (requestListener, checkContinueListener) => {
    var server = http.createServer(requestListener)
    server.listen(config.port, config.host)
    server.on('error', err => {
        var code = err.code
        if (code === 'EACCES') {
            Log.fatal('Starting HTTP server: Access denied')
        }
        if (code === 'EADDRINUSE') {
            Log.fatal('Starting HTTP server: Address already in use')
        }
        if (code === 'EADDRNOTAVAIL') {
            Log.fatal('Starting HTTP server: Address not available')
        }
        throw err
    })
    server.on('checkContinue', checkContinueListener)
}
