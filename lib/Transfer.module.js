module.exports = app => (size, chunkListener, destroyListener) => {

    function startTimeout () {
        abort = app.Timeout(destroyListener, 1000 * 10)
    }

    let left = size
    let abort = () => {}
    let abortListener = null
    let aborted = false
    startTimeout()

    return {
        abort () {
            aborted = true
            if (abortListener === null) abort()
            else abortListener()
        },
        beginFeed (_abortListener) {
            abortListener = _abortListener
            abort()
        },
        endFeed () {
            abortListener = null
            if (left !== 0) startTimeout()
        },
        feed (chunk, deliverCallback) {
            chunkListener(chunk, () => {
                if (aborted) return
                deliverCallback()
                left -= chunk.length
                if (left === 0) destroyListener()
            })
        },
        isBusy () {
            return abortListener !== null
        },
        left () {
            return left
        },
    }

}
