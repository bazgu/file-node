module.exports = app => () => {

    app.LoadConfig(loadedConfig => {

        const url = require('url')

        const chooseSessionNode = app.ChooseSessionNode(loadedConfig)

        const transfers = Object.create(null)

        const pages = Object.create(null)
        pages['/'] = app.IndexPage
        pages['/frontNode/receive'] = app.ReceivePage(chooseSessionNode, transfers)
        app.Static(pages)

        const checkPages = Object.create(null)
        checkPages['/frontNode/feed'] = app.FeedPage(transfers)

        const server = require('http').createServer((req, res) => {

            app.log.info('Http ' + req.method + ' ' + req.url)

            const parsed_url = url.parse(req.url, true)
            const request = {
                parsed_url, req, res,
                respond (response) {
                    app.EchoText(request, {
                        type: 'application/json',
                        content: JSON.stringify(response),
                    })
                },
            }

            const page = (() => {
                const page = pages[parsed_url.pathname]
                if (page !== undefined) return page
                return app.Error404Page
            })()

            page(request)

        }).listen(listen.port, listen.host)
        server.on('checkContinue', (req, res) => {

            app.log.info('Http ' + req.method + ' ' + req.url)

            const parsed_url = url.parse(req.url, true)
            const request = {
                parsed_url, req, res,
                respond (response) {
                    app.EchoText(request, {
                        type: 'application/json',
                        content: JSON.stringify(response),
                    })
                },
            }

            const page = (() => {
                const page = checkPages[parsed_url.pathname]
                if (page !== undefined) return page
                return app.Error404Page
            })()

            page(request)

        })

    })

    app.Watch(['lib', 'static'])

    const listen = app.config.listen

    app.log.info('Started')
    app.log.info('Listening http://' + listen.host + ':' + listen.port + '/')

}
