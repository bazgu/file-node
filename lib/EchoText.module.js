const zlib = require('zlib')

module.exports = app => (request, options) => {

    const content = options.content

    zlib.gzip(content, (err, gzip_content) => {

        const res = request.res
        res.setHeader('Content-Type', options.type)

        ;(plain => {

            if (gzip_content.length >= content.length) {
                plain()
                return
            }

            res.setHeader('Vary', 'Accept-Encoding')

            if (app.AcceptGzip(request.req)) {
                res.setHeader('Content-Encoding', 'gzip')
                res.setHeader('Content-Length', gzip_content.length)
                res.end(gzip_content)
                return
            }

            plain()

        })(() => {
            res.setHeader('Content-Length', Buffer.byteLength(content))
            res.end(content)
        })

    })

}
