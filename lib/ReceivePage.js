var Error500Page = require('./Error500Page.js'),
    Log = require('./Log.js'),
    Transfer = require('./Transfer.js'),
    UnavailablePage = require('./UnavailablePage.js')

module.exports = (chooseSessionNode, transfers) => {
    return (req, res, parsedUrl) => {

        res.setHeader('Content-Type', 'application/json')

        var token = parsedUrl.query.token
        if (token === undefined) {
            res.end('"INVALID_TOKEN"')
            return
        }

        var fetchJson = chooseSessionNode(token)
        if (fetchJson === undefined) {
            UnavailablePage(res)
            return
        }

        ;(() => {

            var abort = fetchJson('/fileNode/requestFile?token=' + encodeURIComponent(token), response => {

                req.removeListener('close', abort)

                if (response === 'INVALID_TOKEN') {
                    UnavailablePage(res)
                    return
                }

                var size = response.size

                res.setHeader('Content-Length', size)
                res.setHeader('Content-Type', 'application/octet-stream')
                res.setHeader('Content-Disposition', 'attachment; filename*=UTF-8\'\'' + encodeURIComponent(response.name))

                if (size === 0) {
                    res.end()
                    return
                }

                ;(() => {

                    function destroy () {
                        delete transfers[sendToken]
                        Log.info(sendToken + ' no longer receiving')
                    }

                    var sendToken = response.sendToken
                    var transfer = transfers[sendToken] = Transfer(size, (chunk, deliverCallback) => {
                        res.write(chunk, deliverCallback)
                    }, () => {
                        res.end()
                        destroy()
                    })
                    Log.info(sendToken + ' receiving')

                    req.on('close', () => {
                        transfer.abort()
                        destroy()
                    })

                })()

            }, () => {
                Error500Page(res)
            })

            req.on('close', abort)

        })()

    }
}
