module.exports = app => request => {
    app.EchoText(request, {
        type: 'text/html; charset=UTF-8',
        content:
            '<!DOCTYPE html>' +
            '<html style="height: 100%">' +
                '<head>' +
                    '<title>File no longer available</title>' +
                    '<meta name="viewport" content="width=device-width, user-scalable=no" />' +
                    '<meta charset="UTF-8" />' +
                    '<style>' +
                        'body {' +
                            'margin: 0;' +
                            'text-align: center;' +
                            'height: 100%;' +
                            'background-color: hsl(95, 70%, 85%);' +
                            'font-size: 15px;' +
                            'line-height: 20px;' +
                            'font-family: Helvetica, Arial, sans-serif;' +
                            'white-space: nowrap;' +
                        '}' +
                        '.aligner {' +
                            'display: inline-block;' +
                            'vertical-align: middle;' +
                            'height: 100%;' +
                        '}' +
                        '.content {' +
                            'display: inline-block;' +
                            'vertical-align: middle;' +
                            'padding: 20px;' +
                            'background-color: white;' +
                            'color: hsl(0, 0%, 30%)' +
                        '}' +
                    '</style>' +
                '</head>' +
                '<body>' +
                    '<div class="aligner"></div>' +
                    '<div class="content">File no longer available</div>' +
                '</body>' +
            '</html>',
    })
}
