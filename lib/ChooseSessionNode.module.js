module.exports = app => loadedConfig => {

    const sessionNodes = loadedConfig.sessionNodes.map(sessionNode => (
        app.FetchJson(sessionNode.host, sessionNode.port)
    ))

    return token => {
        const index = parseInt(token.split('_', 2)[1], 10)
        if (!isFinite(index) || index < 0 || index > sessionNodes.length - 1) return
        return sessionNodes[index]
    }

}
