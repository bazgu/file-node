var methods = [{
    path: 'frontNode/feed',
    queryString: {
        token: {
            type: 'string',
        },
    },
}, {
    path: 'frontNode/receive',
    queryString: {
        token: {
            type: 'string',
        },
    },
}]

var content =
    '<!DOCTYPE html>' +
    '<html>' +
        '<head>' +
            '<title>File Node Documentation</title>' +
            '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />' +
            '<link rel="stylesheet" type="text/css" href="index.css" />' +
        '</head>' +
        '<body>' +
            '<h1 class="title">File Node Documentation</h1>' +
            '<div class="index">' +
                '<h2>Method Reference</h2>' +
                '<ul>' +
                    methods.map(method => {
                        return '<li class="method">' +
                            '<a class="link" href="#' + method.path + '">' +
                                method.path +
                            '</a>' +
                        '</li>'
                    }).join('') +
                '</ul>' +
            '</div>' +
            '<div class="content">' +
                methods.map(method => {
                    var queryString = method.queryString
                    return '<h2 id="' + method.path +'">' +
                            method.path +
                        '</h2>' +
                        '<div style="padding-left: 20px">' +
                            '<div>' +
                                '<h3>Query String</h3>' +
                                '<ul>' +
                                    Object.keys(queryString).map(name => {
                                        var item = queryString[name]
                                        return '<li>' +
                                                '&bull; <code>' + name + ' - (' + item.type + ')</code>' +
                                            '</li>'
                                    }).join('') +
                                '</ul>' +
                            '</div>' +
                        '</div>'
                }).join('<br />') +
            '</div>' +
            '<div style="clear: both"></div>' +
        '</body>' +
    '</html>'

module.exports = (req, res) => {
    res.setHeader('Content-Type', 'text/html; charset=UTF-8')
    res.end(content)
}
