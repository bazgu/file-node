const methods = [{
    path: 'frontNode/feed',
    queryString: {
        token: {
            type: 'string',
        },
    },
}, {
    path: 'frontNode/receive',
    queryString: {
        token: {
            type: 'string',
        },
    },
}]

module.exports = app => request => {
    app.EchoText(request, {
        type: 'text/html; charset=UTF-8',
        content:
            '<!DOCTYPE html>' +
            '<html>' +
                '<head>' +
                    '<title>File Node Documentation</title>' +
                    '<meta charset="UTF-8" />' +
                    '<link rel="stylesheet" href="index.css" />' +
                '</head>' +
                '<body>' +
                    '<h1 class="title">File Node Documentation</h1>' +
                    '<div class="index">' +
                        '<h2>Method Reference</h2>' +
                        '<ul>' +
                            methods.map(method => (
                                '<li class="method">' +
                                    '<a class="link" href="#' + method.path + '">' +
                                        method.path +
                                    '</a>' +
                                '</li>'
                            )).join('') +
                        '</ul>' +
                    '</div>' +
                    '<div class="content">' +
                        methods.map(method => {
                            const queryString = method.queryString
                            return (
                                '<h2 id="' + method.path +'">' +
                                    method.path +
                                '</h2>' +
                                '<div style="padding-left: 20px">' +
                                    '<div>' +
                                        '<h3>Query String</h3>' +
                                        '<ul>' +
                                            Object.keys(queryString).map(name => {
                                                const item = queryString[name]
                                                return (
                                                    '<li>' +
                                                        '&bull; <code>' + name + ' - (' + item.type + ')</code>' +
                                                    '</li>'
                                                )
                                            }).join('') +
                                        '</ul>' +
                                    '</div>' +
                                '</div>'
                            )
                        }).join('<br />') +
                    '</div>' +
                    '<div style="clear: both"></div>' +
                '</body>' +
            '</html>',
    })
}
