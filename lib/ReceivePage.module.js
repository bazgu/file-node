module.exports = app => (chooseSessionNode, transfers) => request => {

    const { req, res } = request

    const token = request.parsed_url.query.token
    if (token === undefined) {
        request.respond('INVALID_TOKEN')
        return
    }

    const fetchJson = chooseSessionNode(token)
    if (fetchJson === undefined) {
        app.UnavailablePage(request)
        return
    }

    ;(() => {

        const abort = fetchJson('/fileNode/requestFile?token=' + encodeURIComponent(token), response => {

            req.removeListener('error', abort)

            if (response === 'INVALID_TOKEN') {
                app.UnavailablePage(request)
                return
            }

            const size = response.size

            res.setHeader('Content-Length', size)
            res.setHeader('Content-Type', 'application/octet-stream')
            res.setHeader('Content-Disposition', 'attachment; filename*=UTF-8\'\'' + encodeURIComponent(response.name))

            if (size === 0) {
                res.end()
                return
            }

            ;(() => {

                function destroy () {
                    delete transfers[sendToken]
                    app.log.info(sendToken + ' no longer receiving')
                }

                const sendToken = response.sendToken
                const transfer = transfers[sendToken] = app.Transfer(size, (chunk, deliverCallback) => {
                    res.write(chunk, deliverCallback)
                }, () => {
                    res.end()
                    destroy()
                })
                app.log.info(sendToken + ' receiving')

                req.on('error', () => {
                    transfer.abort()
                    destroy()
                })

            })()

        }, () => {
            app.Error500Page(request)
        })

        req.on('error', abort)

    })()

}
