process.chdir(__dirname)

require('./lib/LoadConfig.js')(loadedConfig => {

    var version = process.argv[2]

    var url = require('url')

    var config = require('./config.js'),
        Error404Page = require('./lib/Error404Page.js'),
        Log = require('./lib/Log.js')

    var chooseSessionNode = require('./lib/ChooseSessionNode.js')(loadedConfig)

    var transfers = Object.create(null)

    var pages = Object.create(null)
    pages['/'] = require('./lib/IndexPage.js')
    pages['/frontNode/receive'] = require('./lib/ReceivePage.js')(chooseSessionNode, transfers)
    pages['/node'] = require('./lib/NodePage.js')(version)
    require('./lib/ScanFiles.js')('files', pages)

    var checkPages = Object.create(null)
    checkPages['/frontNode/feed'] = require('./lib/FeedPage.js')(transfers)

    require('./lib/Server.js')((req, res) => {
        Log.http(req.method + ' ' + req.url)
        var parsedUrl = url.parse(req.url, true)
        var page = pages[parsedUrl.pathname]
        if (page === undefined) page = Error404Page
        page(req, res, parsedUrl)
    }, (req, res) => {
        Log.http(req.method + ' ' + req.url)
        var parsedUrl = url.parse(req.url, true)
        var page = checkPages[parsedUrl.pathname]
        if (page === undefined) page = Error404Page
        page(req, res, parsedUrl)
    })

})
